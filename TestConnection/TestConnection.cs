﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Ninject;

namespace TestConnection
{
    public enum PingType
    {
        HttpHeaders,
        Ping
    }

    public enum LogerType
    {
        Console,
        File
    }

    public class TestConnection
    {
        private PingType _pingType;
        private LogerType _logerType;
        private ITestConnection _test;
        private ILoger _loger = new ConsoleLoger();

        [Inject]
        public ILoger Loger
        {
            get
            {
                return _loger;
            }
            set
            {
                _loger = value;
            }
        }

        public TestConnection(PingType pingType, LogerType logerType )
        {
            _pingType = pingType;
            _logerType = logerType;
            IoC.Init((kernel) =>
            {
                switch (_pingType)
                {
                    case PingType.HttpHeaders: kernel.Bind<ITestConnection>().To<HttpHeadersTestConnection>().InTransientScope();
                        break;
                    case PingType.Ping: kernel.Bind<ITestConnection>().To<PingTestConnection>().InTransientScope();
                        break;
                }
                if (_logerType == LogerType.File)
                {
                    kernel.Bind<ILoger>().To<FileLoger>().InSingletonScope();

                }
            });
            _test = IoC.Get<ITestConnection>();
        }

        public void Test(string url)
        {
            _test.sendPing(url, Loger);
        }
    }
}
