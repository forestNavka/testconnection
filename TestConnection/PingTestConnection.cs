﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net.NetworkInformation;

namespace TestConnection
{
    public class PingTestConnection : ITestConnection
    {
        public void sendPing(string url, ILoger loger)
        {
            Ping ping = new Ping();
            try
            {
                var result = ping.Send(url);
                loger.log(LogerLevel.Success, result.Status.ToString());
            }
            catch (PingException e)
            {
                loger.log(LogerLevel.Error,e.InnerException.Message);
            }
        }
    }
}
