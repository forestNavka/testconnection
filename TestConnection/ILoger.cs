﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TestConnection
{
    public enum LogerLevel
    {
        Error,
        Success
    }
    
    public interface ILoger
    {
        void log(LogerLevel level, string message);
    }
}