﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using TestConnection;

namespace TestConnection
{
    class HttpHeadersTestConnection : ITestConnection
    {
        public void sendPing(string url, ILoger loger)
        {
            url = "http://" + url;
            HttpWebRequest request = (HttpWebRequest)HttpWebRequest.Create(url);
            request.Timeout = 3000;
            request.AllowAutoRedirect = false;
            request.Method = "HEAD";
            try
            {
                var response = (HttpWebResponse)request.GetResponse();
                loger.log(LogerLevel.Success ,response.Headers.GetValues("Connection").First());
            }
            catch (WebException e)
            {
                loger.log(LogerLevel.Error,e.Message);
                
            }
        }
    }
}
