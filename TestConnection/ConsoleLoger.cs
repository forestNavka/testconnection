﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace TestConnection
{
    public class ConsoleLoger : ILoger
    {

        public void log(LogerLevel level, string message)
        {
            switch (level)
            {
                case LogerLevel.Success: Console.Write("Доступно : ");
                    break;
                case LogerLevel.Error: Console.Write("Помилка : ");
                    break;
            }
            Console.WriteLine(message);
        }
    }
}
