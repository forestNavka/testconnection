﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace TestConnection
{
    public class FileLoger : ILoger
    {

        public void log(LogerLevel level, string message)
        {
            using (StreamWriter writer = new StreamWriter("log.txt"))
            {
                switch (level)
            {
                case LogerLevel.Success: writer.Write("Доступно : ");
                    break;
                case LogerLevel.Error: writer.Write("Помилка : ");
                    break;
            }
            writer.WriteLine(message);
            writer.WriteLine();
            }
            
        }
    }
}
